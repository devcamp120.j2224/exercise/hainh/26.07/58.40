package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Drink;
import com.example.demo.Respository.iDrinkRespository;


@RestController
@CrossOrigin
public class DrinkController {
    
    @Autowired
    iDrinkRespository drinkRespository;

    @GetMapping("demo-drink")
    public ResponseEntity<List <Drink>> getDrink(){
        try {
            List<Drink> listDrink = new ArrayList<Drink>();
            // object. findAll là hàm tìm kiếm tất cả . forEach và add hết tất cả cho arraylist 
            drinkRespository.findAll().forEach(listDrink :: add);

            if(listDrink.size() == 0){
                return new ResponseEntity<List <Drink>>(listDrink, HttpStatus.NOT_FOUND);
            }
            else{
                return new ResponseEntity<List <Drink>>(listDrink, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }    
    }
}
